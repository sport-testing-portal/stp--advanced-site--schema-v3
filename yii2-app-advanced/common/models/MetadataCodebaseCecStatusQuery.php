<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MetadataCodebaseCecStatus]].
 *
 * @see MetadataCodebaseCecStatus
 */
class MetadataCodebaseCecStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataCodebaseCecStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataCodebaseCecStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
