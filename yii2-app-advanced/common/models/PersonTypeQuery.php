<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PersonType]].
 *
 * @see PersonType
 */
class PersonTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PersonType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PersonType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
