<?php

namespace common\models;

use Yii;
use \app\models\base\Coach as BaseCoach;

/**
 * This is the model class for table "coach".
 */
class Coach extends BaseCoach
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'coach_type_id', 'coach_team_coach_id', 'age_group_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri'], 'string', 'max' => 150],
            [['coach_comments'], 'string', 'max' => 253],
            [['coach_info_source_scrape_url'], 'string', 'max' => 100],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'coach_id' => 'Coach ID',
            'person_id' => 'Person ID',
            'coach_type_id' => 'Coach Type ID',
            'coach_team_coach_id' => 'Coach Team Coach ID',
            'age_group_id' => 'Age Group ID',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_comments' => 'Coach Comments',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
            'lock' => 'Lock',
        ];
    }
}
