<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[EventCombineItem]].
 *
 * @see EventCombineItem
 */
class EventCombineItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EventCombineItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EventCombineItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
