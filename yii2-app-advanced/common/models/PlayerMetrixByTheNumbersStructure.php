<?php

namespace common\models;

use Yii;
use \app\models\base\PlayerMetrixByTheNumbersStructure as BasePlayerMetrixByTheNumbersStructure;

/**
 * This is the model class for table "player_metrix_by_the_numbers_structure".
 */
class PlayerMetrixByTheNumbersStructure extends BasePlayerMetrixByTheNumbersStructure
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_age', 'split_cnt'], 'integer'],
            [['split_avg', 'score'], 'number'],
            [['test_date', 'test_desc', 'split_raw'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'person_age' => 'Person Age',
            'test_date' => 'Test Date',
            'test_desc' => 'Test Desc',
            'split_raw' => 'Split Raw',
            'split_avg' => 'Split Avg',
            'split_cnt' => 'Split Cnt',
            'score' => 'Score',
        ];
    }
}
