<?php

namespace common\models;

use Yii;
use \app\models\base\SearchCriteria as BaseSearchCriteria;

/**
 * This is the model class for table "search_criteria".
 */
class SearchCriteria extends BaseSearchCriteria
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['table_name', 'field_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['table_name', 'field_name'], 'string', 'max' => 45],
            [['field_is_indexed_yn', 'lock'], 'string', 'max' => 1],
            [['comments'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'search_criteria_id' => 'Search Criteria ID',
            'table_name' => 'Table Name',
            'field_name' => 'Field Name',
            'field_is_indexed_yn' => 'Field Is Indexed Yn',
            'comments' => 'Comments',
            'lock' => 'Lock',
        ];
    }
}
