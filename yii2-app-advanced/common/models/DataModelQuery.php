<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[DataModel]].
 *
 * @see DataModel
 */
class DataModelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return DataModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DataModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
