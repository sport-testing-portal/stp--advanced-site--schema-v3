<?php

namespace common\models;

use Yii;
use \app\models\base\CampSessionCoach as BaseCampSessionCoach;

/**
 * This is the model class for table "camp_session_coach".
 */
class CampSessionCoach extends BaseCampSessionCoach
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['camp_session_id', 'coach_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_session_coach_id' => 'Camp Session Coach ID',
            'camp_session_id' => 'Camp Session ID',
            'coach_id' => 'Coach ID',
            'lock' => 'Lock',
        ];
    }
}
