<?php

namespace common\models;

use Yii;
use \app\models\base\Player as BasePlayer;

/**
 * This is the model class for table "player".
 */
class Player extends BasePlayer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'age_group_id', 'player_team_player_id', 'player_sport_position_id', 'player_sport_position2_id', 'created_by', 'updated_by'], 'integer'],
            [['player_waiver_minor_dt', 'player_waiver_adult_dt', 'created_at', 'updated_at'], 'safe'],
            [['player_access_code', 'player_sport_position_preference'], 'string', 'max' => 45],
            [['player_parent_email'], 'string', 'max' => 60],
            [['player_sport_preference'], 'string', 'max' => 35],
            [['player_shot_side_preference'], 'string', 'max' => 25],
            [['player_dominant_side', 'player_dominant_foot'], 'string', 'max' => 5],
            [['player_statistical_highlights'], 'string', 'max' => 300],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_id' => 'Player ID',
            'person_id' => 'Person ID',
            'age_group_id' => 'Age Group ID',
            'player_team_player_id' => 'Player Team Player ID',
            'player_sport_position_id' => 'Player Sport Position ID',
            'player_sport_position2_id' => 'Player Sport Position2 ID',
            'player_access_code' => 'Player Access Code',
            'player_waiver_minor_dt' => 'Player Waiver Minor Dt',
            'player_waiver_adult_dt' => 'Player Waiver Adult Dt',
            'player_parent_email' => 'Player Parent Email',
            'player_sport_preference' => 'Player Sport Preference',
            'player_sport_position_preference' => 'Player Sport Position Preference',
            'player_shot_side_preference' => 'Player Shot Side Preference',
            'player_dominant_side' => 'Player Dominant Side',
            'player_dominant_foot' => 'Player Dominant Foot',
            'player_statistical_highlights' => 'Player Statistical Highlights',
            'lock' => 'Lock',
        ];
    }
}
