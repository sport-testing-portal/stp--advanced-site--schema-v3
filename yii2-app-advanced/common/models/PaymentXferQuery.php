<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PaymentXfer]].
 *
 * @see PaymentXfer
 */
class PaymentXferQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PaymentXfer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PaymentXfer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
