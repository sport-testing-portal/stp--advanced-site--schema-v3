<?php

namespace common\models;

use Yii;
use \app\models\base\EntityLink as BaseEntityLink;

/**
 * This is the model class for table "entity_link".
 */
class EntityLink extends BaseEntityLink
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['entity_link_type_id', 'entity1_id', 'entity2_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['entity_link_comment'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'entity_link_id' => 'Entity Link ID',
            'entity_link_type_id' => 'Entity Link Type ID',
            'entity1_id' => 'Entity1 ID',
            'entity2_id' => 'Entity2 ID',
            'entity_link_comment' => 'Entity Link Comment',
            'lock' => 'Lock',
        ];
    }
}
