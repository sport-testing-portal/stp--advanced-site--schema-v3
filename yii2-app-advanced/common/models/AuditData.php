<?php

namespace common\models;

use Yii;
use \app\models\base\AuditData as BaseAuditData;

/**
 * This is the model class for table "audit_data".
 */
class AuditData extends BaseAuditData
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['entry_id', 'type'], 'required'],
            [['entry_id'], 'integer'],
            [['data'], 'string'],
            [['created'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'entry_id' => 'Entry ID',
            'type' => 'Type',
            'data' => 'Data',
        ];
    }
}
