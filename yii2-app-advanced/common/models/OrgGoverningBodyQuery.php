<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[OrgGoverningBody]].
 *
 * @see OrgGoverningBody
 */
class OrgGoverningBodyQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OrgGoverningBody[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrgGoverningBody|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
