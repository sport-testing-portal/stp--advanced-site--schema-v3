<?php

namespace common\models;

use Yii;
use \app\models\base\PlayerCollegeRecruit as BasePlayerCollegeRecruit;

/**
 * This is the model class for table "player_college_recruit".
 */
class PlayerCollegeRecruit extends BasePlayerCollegeRecruit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['player_id', 'school_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_college_recruit_id' => 'Player College Recruit ID',
            'player_id' => 'Player ID',
            'school_id' => 'School ID',
            'lock' => 'Lock',
        ];
    }
}
