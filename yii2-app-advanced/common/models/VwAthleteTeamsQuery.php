<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[VwAthleteTeams]].
 *
 * @see VwAthleteTeams
 */
class VwAthleteTeamsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwAthleteTeams[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwAthleteTeams|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
