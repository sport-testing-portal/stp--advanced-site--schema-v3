<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MetadataCodebaseFunctionItems]].
 *
 * @see MetadataCodebaseFunctionItems
 */
class MetadataCodebaseFunctionItemsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataCodebaseFunctionItems[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataCodebaseFunctionItems|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
