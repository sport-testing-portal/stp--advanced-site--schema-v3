<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[CampSessionSport]].
 *
 * @see CampSessionSport
 */
class CampSessionSportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CampSessionSport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CampSessionSport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
