<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[VwTableRowCountV00]].
 *
 * @see VwTableRowCountV00
 */
class VwTableRowCountV00Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTableRowCountV00[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTableRowCountV00|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
