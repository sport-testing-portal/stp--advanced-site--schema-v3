<?php

namespace common\models;

use Yii;
use \app\models\base\MetadataCodebaseFunction as BaseMetadataCodebaseFunction;

/**
 * This is the model class for table "metadata__codebase_function".
 */
class MetadataCodebaseFunction extends BaseMetadataCodebaseFunction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['codebase_function', 'codebase_function_short_desc'], 'string', 'max' => 45],
            [['codebase_function_long_desc'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'codebase_function_id' => 'Codebase Function ID',
            'codebase_function' => 'Codebase Function',
            'codebase_function_short_desc' => 'Codebase Function Short Desc',
            'codebase_function_long_desc' => 'Codebase Function Long Desc',
            'lock' => 'Lock',
        ];
    }
}
