<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[TestEvalDetailTestDesc]].
 *
 * @see TestEvalDetailTestDesc
 */
class TestEvalDetailTestDescQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TestEvalDetailTestDesc[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TestEvalDetailTestDesc|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
