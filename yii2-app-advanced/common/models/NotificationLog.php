<?php

namespace common\models;

use Yii;
use \app\models\base\NotificationLog as BaseNotificationLog;

/**
 * This is the model class for table "notification_log".
 */
class NotificationLog extends BaseNotificationLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['notification_log_id'], 'required'],
            [['notification_log_id', 'notification_id', 'notification_template_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'notification_log_id' => 'Notification Log ID',
            'notification_id' => 'Notification ID',
            'notification_template_id' => 'Notification Template ID',
            'lock' => 'Lock',
        ];
    }
}
