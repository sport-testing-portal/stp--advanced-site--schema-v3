<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[AuditJavascript]].
 *
 * @see AuditJavascript
 */
class AuditJavascriptQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AuditJavascript[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AuditJavascript|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
