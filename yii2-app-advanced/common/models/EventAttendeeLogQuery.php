<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[EventAttendeeLog]].
 *
 * @see EventAttendeeLog
 */
class EventAttendeeLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EventAttendeeLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EventAttendeeLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
