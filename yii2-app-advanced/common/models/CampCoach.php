<?php

namespace common\models;

use Yii;
use \app\models\base\CampCoach as BaseCampCoach;

/**
 * This is the model class for table "camp_coach".
 */
class CampCoach extends BaseCampCoach
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['camp_id', 'coach_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_coach_id' => 'Camp Coach ID',
            'camp_id' => 'Camp ID',
            'coach_id' => 'Coach ID',
            'lock' => 'Lock',
        ];
    }
}
