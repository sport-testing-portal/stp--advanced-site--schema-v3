<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MetadataCodebaseView]].
 *
 * @see MetadataCodebaseView
 */
class MetadataCodebaseViewQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataCodebaseView[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataCodebaseView|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
