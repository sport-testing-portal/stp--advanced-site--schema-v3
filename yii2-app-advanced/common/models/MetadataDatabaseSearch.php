<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataDatabase;

/**
 * app\models\MetadataDatabaseSearch represents the model behind the search form about `app\models\MetadataDatabase`.
 */
 class MetadataDatabaseSearch extends MetadataDatabase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['database_id', 'col_name_len_max', 'created_by', 'updated_by'], 'integer'],
            [['table_name', 'schema_name', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataDatabase::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'database_id' => $this->database_id,
            'col_name_len_max' => $this->col_name_len_max,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'table_name', $this->table_name])
            ->andFilterWhere(['like', 'schema_name', $this->schema_name])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
