<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Camp]].
 *
 * @see Camp
 */
class CampQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Camp[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Camp|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
