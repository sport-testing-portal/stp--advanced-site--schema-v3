<?php

namespace common\models;

use Yii;
use \app\models\base\PaymentXfer as BasePaymentXfer;

/**
 * This is the model class for table "payment_xfer".
 */
class PaymentXfer extends BasePaymentXfer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['payment_log_id', 'payment_xfer_entity_id', 'payment_xfer_status_code_num', 'created_by', 'updated_by'], 'integer'],
            [['payment_xfer_sent_dt', 'payment_xfer_recd_status_dt', 'created_at', 'updated_at'], 'safe'],
            [['payment_xfer_status_code_char', 'payment_xfer_blob_format'], 'string', 'max' => 45],
            [['payment_xfer_blob_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'payment_xfer_id' => 'Payment Xfer ID',
            'payment_log_id' => 'Payment Log ID',
            'payment_xfer_entity_id' => 'Payment Xfer Entity ID',
            'payment_xfer_status_code_char' => 'Payment Xfer Status Code Char',
            'payment_xfer_status_code_num' => 'Payment Xfer Status Code Num',
            'payment_xfer_sent_dt' => 'Payment Xfer Sent Dt',
            'payment_xfer_recd_status_dt' => 'Payment Xfer Recd Status Dt',
            'payment_xfer_blob_uri' => 'Payment Xfer Blob Uri',
            'payment_xfer_blob_format' => 'Payment Xfer Blob Format',
            'lock' => 'Lock',
        ];
    }
}
