<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[SearchHistory]].
 *
 * @see SearchHistory
 */
class SearchHistoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SearchHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SearchHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
