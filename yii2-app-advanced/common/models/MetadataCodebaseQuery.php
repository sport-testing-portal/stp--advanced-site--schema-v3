<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[MetadataCodebase]].
 *
 * @see MetadataCodebase
 */
class MetadataCodebaseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataCodebase[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataCodebase|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
