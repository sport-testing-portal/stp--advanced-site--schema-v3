<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ImportFile]].
 *
 * @see ImportFile
 */
class ImportFileQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ImportFile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ImportFile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
