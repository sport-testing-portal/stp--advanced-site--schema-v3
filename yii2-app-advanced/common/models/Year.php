<?php

namespace common\models;

use Yii;
use \app\models\base\Year as BaseYear;

/**
 * This is the model class for table "year".
 */
class Year extends BaseYear
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['year'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'year_id' => 'Year ID',
            'year' => 'Year',
        ];
    }
}
