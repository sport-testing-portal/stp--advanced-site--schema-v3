<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[VwSiteOperations]].
 *
 * @see VwSiteOperations
 */
class VwSiteOperationsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwSiteOperations[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwSiteOperations|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
