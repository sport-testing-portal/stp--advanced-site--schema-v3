<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "test_eval_detail_log".
 *
 * @property integer $test_eval_detail_log_id
 * @property integer $test_eval_summary_log_id
 * @property integer $test_eval_detail_test_order_id
 * @property integer $test_eval_detail_test_desc_id
 * @property string $test_eval_detail_score_text
 * @property string $test_eval_detail_score_num
 * @property integer $test_eval_detail_attempt
 * @property integer $test_eval_detail_score_int
 * @property string $test_eval_detail_score_url
 * @property string $test_eval_detail_video_url
 * @property integer $test_eval_detail_rating
 * @property integer $test_eval_detail_overall_ranking
 * @property integer $test_eval_detail_positional_ranking
 * @property integer $test_eval_detail_source_event_id
 * @property integer $test_eval_detail_source_record_id
 * @property integer $test_eval_detail_import_file_id
 * @property integer $test_eval_detail_import_file_line_num
 * @property string $test_eval_detail_trial_status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\TestEvalDetailTestDesc $testEvalDetailTestDesc
 * @property \app\models\TestEvalSummaryLog $testEvalSummaryLog
 */
class TestEvalDetailLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'testEvalDetailTestDesc',
            'testEvalSummaryLog'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_eval_summary_log_id', 'test_eval_detail_test_order_id', 'test_eval_detail_test_desc_id', 'test_eval_detail_score_int', 'test_eval_detail_rating', 'test_eval_detail_overall_ranking', 'test_eval_detail_positional_ranking', 'test_eval_detail_source_event_id', 'test_eval_detail_source_record_id', 'test_eval_detail_import_file_id', 'test_eval_detail_import_file_line_num', 'created_by', 'updated_by'], 'integer'],
            [['test_eval_detail_score_num'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['test_eval_detail_score_text', 'test_eval_detail_trial_status'], 'string', 'max' => 45],
            [['test_eval_detail_attempt'], 'string', 'max' => 4],
            [['test_eval_detail_score_url', 'test_eval_detail_video_url'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_eval_detail_log';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'test_eval_detail_log_id' => 'Test Eval Detail Log ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'test_eval_detail_test_order_id' => 'Test Eval Detail Test Order ID',
            'test_eval_detail_test_desc_id' => 'Test Eval Detail Test Desc ID',
            'test_eval_detail_score_text' => 'Test Eval Detail Score Text',
            'test_eval_detail_score_num' => 'Test Eval Detail Score Num',
            'test_eval_detail_attempt' => 'Test Eval Detail Attempt',
            'test_eval_detail_score_int' => 'Test Eval Detail Score Int',
            'test_eval_detail_score_url' => 'Test Eval Detail Score Url',
            'test_eval_detail_video_url' => 'Test Eval Detail Video Url',
            'test_eval_detail_rating' => 'Test Eval Detail Rating',
            'test_eval_detail_overall_ranking' => 'Test Eval Detail Overall Ranking',
            'test_eval_detail_positional_ranking' => 'Test Eval Detail Positional Ranking',
            'test_eval_detail_source_event_id' => 'Test Eval Detail Source Event ID',
            'test_eval_detail_source_record_id' => 'Test Eval Detail Source Record ID',
            'test_eval_detail_import_file_id' => 'Test Eval Detail Import File ID',
            'test_eval_detail_import_file_line_num' => 'Test Eval Detail Import File Line Num',
            'test_eval_detail_trial_status' => 'Test Eval Detail Trial Status',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalDetailTestDesc()
    {
        return $this->hasOne(\app\models\TestEvalDetailTestDesc::className(), ['test_eval_detail_test_desc_id' => 'test_eval_detail_test_desc_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalSummaryLog()
    {
        return $this->hasOne(\app\models\TestEvalSummaryLog::className(), ['test_eval_summary_log_id' => 'test_eval_summary_log_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TestEvalDetailLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TestEvalDetailLogQuery(get_called_class());
    }
}
