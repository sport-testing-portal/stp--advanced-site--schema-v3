<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "test_eval_log".
 *
 * @property integer $test_eval_log_id
 * @property integer $test_eval_type_id
 * @property integer $test_eval_summary_log_id
 * @property integer $rfid
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\TestEvalSummaryLog $testEvalSummaryLog
 * @property \app\models\TestEvalType $testEvalType
 */
class TestEvalLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'testEvalSummaryLog',
            'testEvalType'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_eval_type_id', 'test_eval_summary_log_id', 'rfid', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_eval_log';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'test_eval_log_id' => 'Test Eval Log ID',
            'test_eval_type_id' => 'Test Eval Type ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'rfid' => 'Rfid',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalSummaryLog()
    {
        return $this->hasOne(\app\models\TestEvalSummaryLog::className(), ['test_eval_summary_log_id' => 'test_eval_summary_log_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalType()
    {
        return $this->hasOne(\app\models\TestEvalType::className(), ['test_eval_type_id' => 'test_eval_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TestEvalLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TestEvalLogQuery(get_called_class());
    }
}
