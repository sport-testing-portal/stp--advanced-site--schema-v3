<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "school_stat".
 *
 * @property integer $school_stat_id
 * @property integer $school_id
 * @property integer $college_cost_rating
 * @property integer $college_cost_tuition_instate
 * @property integer $college_cost_tuition_outofstate
 * @property integer $college_cost_room_and_board
 * @property integer $college_cost_books_and_supplies
 * @property string $college_early_decision_dt
 * @property string $college_early_action_dt
 * @property string $college_regular_decision_dt
 * @property integer $college_pct_applicants_admitted
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\School $school
 */
class SchoolStat extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'school'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'college_cost_rating', 'college_cost_tuition_instate', 'college_cost_tuition_outofstate', 'college_cost_room_and_board', 'college_cost_books_and_supplies', 'college_pct_applicants_admitted', 'created_by', 'updated_by'], 'integer'],
            [['college_early_decision_dt', 'college_early_action_dt', 'college_regular_decision_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_stat';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_stat_id' => 'School Stat ID',
            'school_id' => 'School ID',
            'college_cost_rating' => 'College Cost Rating',
            'college_cost_tuition_instate' => 'College Cost Tuition Instate',
            'college_cost_tuition_outofstate' => 'College Cost Tuition Outofstate',
            'college_cost_room_and_board' => 'College Cost Room And Board',
            'college_cost_books_and_supplies' => 'College Cost Books And Supplies',
            'college_early_decision_dt' => 'College Early Decision Dt',
            'college_early_action_dt' => 'College Early Action Dt',
            'college_regular_decision_dt' => 'College Regular Decision Dt',
            'college_pct_applicants_admitted' => 'College Pct Applicants Admitted',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\app\models\School::className(), ['school_id' => 'school_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SchoolStatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SchoolStatQuery(get_called_class());
    }
}
