<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "camp_session_coach".
 *
 * @property integer $camp_session_coach_id
 * @property integer $camp_session_id
 * @property integer $coach_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Coach $coach
 * @property \app\models\CampSession $campSession
 */
class CampSessionCoach extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'coach',
            'campSession'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_session_id', 'coach_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'camp_session_coach';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'camp_session_coach_id' => 'Camp Session Coach ID',
            'camp_session_id' => 'Camp Session ID',
            'coach_id' => 'Coach ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoach()
    {
        return $this->hasOne(\app\models\Coach::className(), ['coach_id' => 'coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSession()
    {
        return $this->hasOne(\app\models\CampSession::className(), ['camp_session_id' => 'camp_session_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\CampSessionCoachQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CampSessionCoachQuery(get_called_class());
    }
}
