<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "player".
 *
 * @property integer $player_id
 * @property integer $person_id
 * @property integer $age_group_id
 * @property integer $player_team_player_id
 * @property integer $player_sport_position_id
 * @property integer $player_sport_position2_id
 * @property string $player_access_code
 * @property string $player_waiver_minor_dt
 * @property string $player_waiver_adult_dt
 * @property string $player_parent_email
 * @property string $player_sport_preference
 * @property string $player_sport_position_preference
 * @property string $player_shot_side_preference
 * @property string $player_dominant_side
 * @property string $player_dominant_foot
 * @property string $player_statistical_highlights
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\CampSessionPlayerEval[] $campSessionPlayerEvals
 * @property \app\models\AgeGroup $ageGroup
 * @property \app\models\Person $person
 * @property \app\models\SportPosition $playerSportPosition
 * @property \app\models\TeamPlayer $playerTeamPlayer
 * @property \app\models\PlayerAcademic[] $playerAcademics
 * @property \app\models\PlayerCampLog[] $playerCampLogs
 * @property \app\models\PlayerCollegeRecruit[] $playerCollegeRecruits
 * @property \app\models\PlayerContact[] $playerContacts
 * @property \app\models\PlayerSchool[] $playerSchools
 * @property \app\models\PlayerSport[] $playerSports
 * @property \app\models\PlayerTeam[] $playerTeams
 * @property \app\models\TeamPlayer[] $teamPlayers
 */
class Player extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'campSessionPlayerEvals',
            'ageGroup',
            'person',
            'playerSportPosition',
            'playerTeamPlayer',
            'playerAcademics',
            'playerCampLogs',
            'playerCollegeRecruits',
            'playerContacts',
            'playerSchools',
            'playerSports',
            'playerTeams',
            'teamPlayers'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'age_group_id', 'player_team_player_id', 'player_sport_position_id', 'player_sport_position2_id', 'created_by', 'updated_by'], 'integer'],
            [['player_waiver_minor_dt', 'player_waiver_adult_dt', 'created_at', 'updated_at'], 'safe'],
            [['player_access_code', 'player_sport_position_preference'], 'string', 'max' => 45],
            [['player_parent_email'], 'string', 'max' => 60],
            [['player_sport_preference'], 'string', 'max' => 35],
            [['player_shot_side_preference'], 'string', 'max' => 25],
            [['player_dominant_side', 'player_dominant_foot'], 'string', 'max' => 5],
            [['player_statistical_highlights'], 'string', 'max' => 300],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'player_id' => 'Player ID',
            'person_id' => 'Person ID',
            'age_group_id' => 'Age Group ID',
            'player_team_player_id' => 'Player Team Player ID',
            'player_sport_position_id' => 'Player Sport Position ID',
            'player_sport_position2_id' => 'Player Sport Position2 ID',
            'player_access_code' => 'Player Access Code',
            'player_waiver_minor_dt' => 'Player Waiver Minor Dt',
            'player_waiver_adult_dt' => 'Player Waiver Adult Dt',
            'player_parent_email' => 'Player Parent Email',
            'player_sport_preference' => 'Player Sport Preference',
            'player_sport_position_preference' => 'Player Sport Position Preference',
            'player_shot_side_preference' => 'Player Shot Side Preference',
            'player_dominant_side' => 'Player Dominant Side',
            'player_dominant_foot' => 'Player Dominant Foot',
            'player_statistical_highlights' => 'Player Statistical Highlights',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionPlayerEvals()
    {
        return $this->hasMany(\app\models\CampSessionPlayerEval::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeGroup()
    {
        return $this->hasOne(\app\models\AgeGroup::className(), ['age_group_id' => 'age_group_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerSportPosition()
    {
        return $this->hasOne(\app\models\SportPosition::className(), ['sport_position_id' => 'player_sport_position_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerTeamPlayer()
    {
        return $this->hasOne(\app\models\TeamPlayer::className(), ['team_player_id' => 'player_team_player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerAcademics()
    {
        return $this->hasMany(\app\models\PlayerAcademic::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerCampLogs()
    {
        return $this->hasMany(\app\models\PlayerCampLog::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerCollegeRecruits()
    {
        return $this->hasMany(\app\models\PlayerCollegeRecruit::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerContacts()
    {
        return $this->hasMany(\app\models\PlayerContact::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerSchools()
    {
        return $this->hasMany(\app\models\PlayerSchool::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerSports()
    {
        return $this->hasMany(\app\models\PlayerSport::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerTeams()
    {
        return $this->hasMany(\app\models\PlayerTeam::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamPlayers()
    {
        return $this->hasMany(\app\models\TeamPlayer::className(), ['player_id' => 'player_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PlayerQuery(get_called_class());
    }
}
