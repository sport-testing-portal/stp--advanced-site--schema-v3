<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "team_coach".
 *
 * @property integer $team_coach_id
 * @property integer $team_id
 * @property integer $coach_id
 * @property integer $team_coach_coach_type_id
 * @property string $primary_position
 * @property string $team_coach_begin_dt
 * @property string $team_coach_end_dt
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Coach[] $coaches
 * @property \app\models\Coach $coach
 * @property \app\models\Team $team
 */
class TeamCoach extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'coaches',
            'coach',
            'team'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'coach_id'], 'required'],
            [['team_id', 'coach_id', 'team_coach_coach_type_id', 'created_by', 'updated_by'], 'integer'],
            [['team_coach_begin_dt', 'team_coach_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['primary_position'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['team_id', 'coach_id'], 'unique', 'targetAttribute' => ['team_id', 'coach_id'], 'message' => 'The combination of Team ID and Coach ID has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team_coach';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_coach_id' => 'Team Coach ID',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'team_coach_coach_type_id' => 'Team Coach Coach Type ID',
            'primary_position' => 'Primary Position',
            'team_coach_begin_dt' => 'Team Coach Begin Dt',
            'team_coach_end_dt' => 'Team Coach End Dt',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoaches()
    {
        return $this->hasMany(\app\models\Coach::className(), ['coach_team_coach_id' => 'team_coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoach()
    {
        return $this->hasOne(\app\models\Coach::className(), ['coach_id' => 'coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(\app\models\Team::className(), ['team_id' => 'team_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TeamCoachQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TeamCoachQuery(get_called_class());
    }
}
