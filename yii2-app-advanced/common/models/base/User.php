<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $activation_key
 * @property string $created_on
 * @property string $updated_on
 * @property string $last_visit_on
 * @property string $password_set_on
 * @property integer $email_verified
 * @property integer $is_active
 * @property integer $is_disabled
 * @property string $one_time_password_secret
 * @property string $one_time_password_code
 * @property integer $one_time_password_counter
 * @property string $srbac_set_on
 * @property string $person_addr_1
 * @property string $person_addr_2
 * @property string $person_city
 * @property string $person_state_or_region
 * @property string $person_postal_code
 * @property integer $gender_id
 * @property string $person_date_of_birth
 * @property string $password_hash
 * @property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property integer $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 * @property integer $last_login_at
 *
 * @property \app\models\AppUser $appUser
 * @property \app\models\AppUserDevice[] $appUserDevices
 * @property \app\models\AppUserLocation[] $appUserLocations
 * @property \app\models\PlayerResultsSearchLog[] $playerResultsSearchLogs
 * @property \app\models\Profile $profile
 * @property \app\models\SocialAccount[] $socialAccounts
 * @property \app\models\Token[] $tokens
 * @property \app\models\UserLoginAttempt[] $userLoginAttempts
 * @property \app\models\UserProfilePicture[] $userProfilePictures
 * @property \app\models\UserRemoteIdentity[] $userRemoteIdentities
 * @property \app\models\UserUsedPassword[] $userUsedPasswords
 */
class User extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'appUser',
            'appUserDevices',
            'appUserLocations',
            'playerResultsSearchLogs',
            'profile',
            'socialAccounts',
            'tokens',
            'userLoginAttempts',
            'userProfilePictures',
            'userRemoteIdentities',
            'userUsedPasswords'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password_hash', 'auth_key', 'created_at', 'updated_at'], 'required'],
            [['created_on', 'updated_on', 'last_visit_on', 'password_set_on', 'srbac_set_on', 'person_date_of_birth'], 'safe'],
            [['one_time_password_counter', 'gender_id', 'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags', 'last_login_at'], 'integer'],
            [['username', 'password', 'email', 'activation_key', 'one_time_password_secret', 'one_time_password_code', 'unconfirmed_email'], 'string', 'max' => 255],
            [['firstname', 'lastname', 'person_addr_1', 'person_addr_2', 'person_city'], 'string', 'max' => 65],
            [['email_verified', 'is_active', 'is_disabled'], 'string', 'max' => 4],
            [['person_state_or_region', 'registration_ip'], 'string', 'max' => 45],
            [['person_postal_code'], 'string', 'max' => 18],
            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'activation_key' => 'Activation Key',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'last_visit_on' => 'Last Visit On',
            'password_set_on' => 'Password Set On',
            'email_verified' => 'Email Verified',
            'is_active' => 'Is Active',
            'is_disabled' => 'Is Disabled',
            'one_time_password_secret' => 'One Time Password Secret',
            'one_time_password_code' => 'One Time Password Code',
            'one_time_password_counter' => 'One Time Password Counter',
            'srbac_set_on' => 'Srbac Set On',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_city' => 'Person City',
            'person_state_or_region' => 'Person State Or Region',
            'person_postal_code' => 'Person Postal Code',
            'gender_id' => 'Gender ID',
            'person_date_of_birth' => 'Person Date Of Birth',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'confirmed_at' => 'Confirmed At',
            'unconfirmed_email' => 'Unconfirmed Email',
            'blocked_at' => 'Blocked At',
            'registration_ip' => 'Registration Ip',
            'flags' => 'Flags',
            'last_login_at' => 'Last Login At',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUser()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['app_user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserDevices()
    {
        return $this->hasMany(\app\models\AppUserDevice::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserLocations()
    {
        return $this->hasMany(\app\models\AppUserLocation::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerResultsSearchLogs()
    {
        return $this->hasMany(\app\models\PlayerResultsSearchLog::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(\app\models\Profile::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialAccounts()
    {
        return $this->hasMany(\app\models\SocialAccount::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(\app\models\Token::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLoginAttempts()
    {
        return $this->hasMany(\app\models\UserLoginAttempt::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfilePictures()
    {
        return $this->hasMany(\app\models\UserProfilePicture::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRemoteIdentities()
    {
        return $this->hasMany(\app\models\UserRemoteIdentity::className(), ['user_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUsedPasswords()
    {
        return $this->hasMany(\app\models\UserUsedPassword::className(), ['user_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\UserQuery(get_called_class());
    }
}
