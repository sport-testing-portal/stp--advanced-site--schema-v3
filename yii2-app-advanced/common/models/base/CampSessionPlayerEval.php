<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "camp_session_player_eval".
 *
 * @property integer $camp_session_player_eval_id
 * @property integer $camp_session_id
 * @property integer $coach_id
 * @property integer $player_id
 * @property string $camp_session_coach_comment
 * @property string $camp_session_player_comment
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\CampSession $campSession
 * @property \app\models\Coach $coach
 * @property \app\models\Player $player
 */
class CampSessionPlayerEval extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'campSession',
            'coach',
            'player'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_session_id', 'coach_id', 'player_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['camp_session_coach_comment', 'camp_session_player_comment'], 'string', 'max' => 450],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'camp_session_player_eval';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'camp_session_player_eval_id' => 'Camp Session Player Eval ID',
            'camp_session_id' => 'Camp Session ID',
            'coach_id' => 'Coach ID',
            'player_id' => 'Player ID',
            'camp_session_coach_comment' => 'Camp Session Coach Comment',
            'camp_session_player_comment' => 'Camp Session Player Comment',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSession()
    {
        return $this->hasOne(\app\models\CampSession::className(), ['camp_session_id' => 'camp_session_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoach()
    {
        return $this->hasOne(\app\models\Coach::className(), ['coach_id' => 'coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(\app\models\Player::className(), ['player_id' => 'player_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\CampSessionPlayerEvalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CampSessionPlayerEvalQuery(get_called_class());
    }
}
