<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "school_act".
 *
 * @property integer $school_act_id
 * @property integer $school_id
 * @property integer $school_unit_id
 * @property integer $school_act_composite_25_pct
 * @property integer $school_act_composite_75_pct
 * @property integer $school_act_english_25_pct
 * @property integer $school_act_english_75_pct
 * @property integer $school_act_math_25_pct
 * @property integer $school_act_math_75_pct
 * @property integer $school_act_student_submit_cnt
 * @property string $school_act_student_submit_pct
 * @property string $school_act_report_period
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\School $school
 */
class SchoolAct extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'school'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'school_unit_id', 'school_act_composite_25_pct', 'school_act_composite_75_pct', 'school_act_english_25_pct', 'school_act_english_75_pct', 'school_act_math_25_pct', 'school_act_math_75_pct', 'school_act_student_submit_cnt', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['school_act_student_submit_pct'], 'string', 'max' => 5],
            [['school_act_report_period'], 'string', 'max' => 12],
            [['lock'], 'string', 'max' => 1],
            [['school_unit_id'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_act';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_act_id' => 'School Act ID',
            'school_id' => 'School ID',
            'school_unit_id' => 'School Unit ID',
            'school_act_composite_25_pct' => 'School Act Composite 25 Pct',
            'school_act_composite_75_pct' => 'School Act Composite 75 Pct',
            'school_act_english_25_pct' => 'School Act English 25 Pct',
            'school_act_english_75_pct' => 'School Act English 75 Pct',
            'school_act_math_25_pct' => 'School Act Math 25 Pct',
            'school_act_math_75_pct' => 'School Act Math 75 Pct',
            'school_act_student_submit_cnt' => 'School Act Student Submit Cnt',
            'school_act_student_submit_pct' => 'School Act Student Submit Pct',
            'school_act_report_period' => 'School Act Report Period',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\app\models\School::className(), ['school_id' => 'school_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SchoolActQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SchoolActQuery(get_called_class());
    }
}
