<?php

namespace common\models;

use Yii;
use \app\models\base\VwTestDescription as BaseVwTestDescription;

/**
 * This is the model class for table "vwTestDescription".
 */
class VwTestDescription extends BaseVwTestDescription
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['test_category'], 'required'],
            [['provider_id', 'cat_id', 'type_id', 'desc_id', 'cat_order', 'type_order', 'desc_order'], 'integer'],
            [['test_category', 'test_desc'], 'string', 'max' => 45],
            [['test_type'], 'string', 'max' => 150],
            [['test_desc_to_display'], 'string', 'max' => 131],
            [['provider_code'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'test_category' => 'Test Category',
            'test_type' => 'Test Type',
            'test_desc' => 'Test Desc',
            'provider_id' => 'Provider ID',
            'cat_id' => 'Cat ID',
            'type_id' => 'Type ID',
            'desc_id' => 'Desc ID',
            'cat_order' => 'Cat Order',
            'type_order' => 'Type Order',
            'desc_order' => 'Desc Order',
            'test_desc_to_display' => 'Test Desc To Display',
            'provider_code' => 'Provider Code',
        ];
    }
}
