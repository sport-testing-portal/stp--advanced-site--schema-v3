<?php

namespace common\models;

use Yii;
use \app\models\base\PlayerContact as BasePlayerContact;

/**
 * This is the model class for table "player_contact".
 */
class PlayerContact extends BasePlayerContact
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['player_contact_type_id', 'player_id', 'person_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_contact_id' => 'Player Contact ID',
            'player_contact_type_id' => 'Player Contact Type ID',
            'player_id' => 'Player ID',
            'person_id' => 'Person ID',
            'lock' => 'Lock',
        ];
    }
}
