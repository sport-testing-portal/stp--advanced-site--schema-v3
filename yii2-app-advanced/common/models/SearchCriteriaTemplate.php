<?php

namespace common\models;

use Yii;
use \app\models\base\SearchCriteriaTemplate as BaseSearchCriteriaTemplate;

/**
 * This is the model class for table "search_criteria_template".
 */
class SearchCriteriaTemplate extends BaseSearchCriteriaTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['template_name'], 'string', 'max' => 75],
            [['template_comment'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'search_criteria_template_id' => 'Search Criteria Template ID',
            'person_id' => 'Person ID',
            'template_name' => 'Template Name',
            'template_comment' => 'Template Comment',
            'lock' => 'Lock',
        ];
    }
}
