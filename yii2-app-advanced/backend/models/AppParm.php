<?php

namespace app\models;

use Yii;
use \app\models\base\AppParm as BaseAppParm;

/**
 * This is the model class for table "app_parm".
 */
class AppParm extends BaseAppParm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['ap_int', 'ap_display_order', 'updated_by', 'created_by'], 'integer'],
            [['ap_datetime', 'updated_at', 'created_at'], 'safe'],
            [['app_parm_type'], 'string', 'max' => 100],
            [['ap_varchar', 'ap_comment'], 'string', 'max' => 1024],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_parm_id' => 'App Parm ID',
            'app_parm_type' => 'App Parm Type',
            'ap_int' => 'Ap Int',
            'ap_varchar' => 'Ap Varchar',
            'ap_datetime' => 'Ap Datetime',
            'ap_comment' => 'Ap Comment',
            'ap_display_order' => 'Ap Display Order',
            'lock' => 'Lock',
        ];
    }
}
