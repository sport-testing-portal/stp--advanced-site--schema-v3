<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PaymentLog]].
 *
 * @see PaymentLog
 */
class PaymentLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PaymentLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PaymentLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
