<?php

namespace app\models;

use Yii;
use \app\models\base\UserUsedPassword as BaseUserUsedPassword;

/**
 * This is the model class for table "user_used_password".
 */
class UserUsedPassword extends BaseUserUsedPassword
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'password'], 'required'],
            [['user_id'], 'integer'],
            [['set_on'], 'safe'],
            [['password'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'password' => 'Password',
            'set_on' => 'Set On',
        ];
    }
}
