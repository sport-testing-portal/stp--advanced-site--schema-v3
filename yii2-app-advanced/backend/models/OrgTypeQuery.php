<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OrgType]].
 *
 * @see OrgType
 */
class OrgTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OrgType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrgType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
