<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ReligiousAffiliation]].
 *
 * @see ReligiousAffiliation
 */
class ReligiousAffiliationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ReligiousAffiliation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ReligiousAffiliation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
