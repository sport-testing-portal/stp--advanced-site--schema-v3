<?php

namespace app\models;

use Yii;
use \app\models\base\TeamCoach as BaseTeamCoach;

/**
 * This is the model class for table "team_coach".
 */
class TeamCoach extends BaseTeamCoach
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['team_id', 'coach_id'], 'required'],
            [['team_id', 'coach_id', 'team_coach_coach_type_id', 'created_by', 'updated_by'], 'integer'],
            [['team_coach_begin_dt', 'team_coach_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['primary_position'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['team_id', 'coach_id'], 'unique', 'targetAttribute' => ['team_id', 'coach_id'], 'message' => 'The combination of Team ID and Coach ID has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'team_coach_id' => 'Team Coach ID',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'team_coach_coach_type_id' => 'Team Coach Coach Type ID',
            'primary_position' => 'Primary Position',
            'team_coach_begin_dt' => 'Team Coach Begin Dt',
            'team_coach_end_dt' => 'Team Coach End Dt',
            'lock' => 'Lock',
        ];
    }
}
