<?php

namespace app\models;

use Yii;
use \app\models\base\PlayerSport as BasePlayerSport;

/**
 * This is the model class for table "player_sport".
 */
class PlayerSport extends BasePlayerSport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['player_id', 'sport_id', 'sport_position_id', 'gender_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_sport_id' => 'Player Sport ID',
            'player_id' => 'Player ID',
            'sport_id' => 'Sport ID',
            'sport_position_id' => 'Sport Position ID',
            'gender_id' => 'Gender ID',
            'lock' => 'Lock',
        ];
    }
}
