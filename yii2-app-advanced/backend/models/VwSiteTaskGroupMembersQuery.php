<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwSiteTaskGroupMembers]].
 *
 * @see VwSiteTaskGroupMembers
 */
class VwSiteTaskGroupMembersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwSiteTaskGroupMembers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwSiteTaskGroupMembers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
