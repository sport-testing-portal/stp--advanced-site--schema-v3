<?php

namespace app\models;

use Yii;
use \app\models\base\CampSession as BaseCampSession;

/**
 * This is the model class for table "camp_session".
 */
class CampSession extends BaseCampSession
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['camp_id', 'sport_id', 'gender_id', 'address_id', 'season_id', 'camp_session_cost_regular_residential', 'camp_session_cost_regular_commuter', 'camp_session_cost_regular_day', 'camp_session_cost_early_residential', 'camp_session_cost_early_commuter', 'camp_session_cost_early_day', 'created_by', 'updated_by'], 'integer'],
            [['camp_session_begin_dt', 'camp_session_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['camp_session_location', 'camp_session_url', 'camp_session_ages', 'camp_session_skill_level', 'camp_session_type', 'camp_session_specialty', 'camp_session_description'], 'string', 'max' => 45],
            [['camp_session_scholarships_available_yn', 'lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_session_id' => 'Camp Session ID',
            'camp_id' => 'Camp ID',
            'sport_id' => 'Sport ID',
            'gender_id' => 'Gender ID',
            'address_id' => 'Address ID',
            'season_id' => 'Season ID',
            'camp_session_location' => 'Camp Session Location',
            'camp_session_url' => 'Camp Session Url',
            'camp_session_ages' => 'Camp Session Ages',
            'camp_session_skill_level' => 'Camp Session Skill Level',
            'camp_session_type' => 'Camp Session Type',
            'camp_session_begin_dt' => 'Camp Session Begin Dt',
            'camp_session_end_dt' => 'Camp Session End Dt',
            'camp_session_specialty' => 'Camp Session Specialty',
            'camp_session_scholarships_available_yn' => 'Camp Session Scholarships Available Yn',
            'camp_session_description' => 'Camp Session Description',
            'camp_session_cost_regular_residential' => 'Camp Session Cost Regular Residential',
            'camp_session_cost_regular_commuter' => 'Camp Session Cost Regular Commuter',
            'camp_session_cost_regular_day' => 'Camp Session Cost Regular Day',
            'camp_session_cost_early_residential' => 'Camp Session Cost Early Residential',
            'camp_session_cost_early_commuter' => 'Camp Session Cost Early Commuter',
            'camp_session_cost_early_day' => 'Camp Session Cost Early Day',
            'lock' => 'Lock',
        ];
    }
}
