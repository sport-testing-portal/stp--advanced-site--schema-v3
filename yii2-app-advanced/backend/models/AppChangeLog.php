<?php

namespace app\models;

use Yii;
use \app\models\base\AppChangeLog as BaseAppChangeLog;

/**
 * This is the model class for table "app_change_log".
 */
class AppChangeLog extends BaseAppChangeLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['app_semantic_version'], 'string', 'max' => 45],
            [['app_change_desc'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_change_log_id' => 'App Change Log ID',
            'app_semantic_version' => 'App Semantic Version',
            'app_change_desc' => 'App Change Desc',
            'lock' => 'Lock',
        ];
    }
}
