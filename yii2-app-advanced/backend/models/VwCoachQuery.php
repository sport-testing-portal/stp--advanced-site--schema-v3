<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwCoach]].
 *
 * @see VwCoach
 */
class VwCoachQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwCoach[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwCoach|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
