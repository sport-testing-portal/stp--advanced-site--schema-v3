<?php

namespace app\models;

use Yii;
use \app\models\base\Height as BaseHeight;

/**
 * This is the model class for table "height".
 */
class Height extends BaseHeight
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['in', 'cm', 'hands'], 'integer'],
            [['english_long', 'english_short'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'height_id' => 'Height ID',
            'english_long' => 'English Long',
            'english_short' => 'English Short',
            'in' => 'In',
            'cm' => 'Cm',
            'hands' => 'Hands',
        ];
    }
}
