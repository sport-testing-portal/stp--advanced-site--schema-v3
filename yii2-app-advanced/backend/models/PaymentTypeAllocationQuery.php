<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PaymentTypeAllocation]].
 *
 * @see PaymentTypeAllocation
 */
class PaymentTypeAllocationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PaymentTypeAllocation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PaymentTypeAllocation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
