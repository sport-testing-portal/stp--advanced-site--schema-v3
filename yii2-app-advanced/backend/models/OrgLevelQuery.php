<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OrgLevel]].
 *
 * @see OrgLevel
 */
class OrgLevelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OrgLevel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrgLevel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
