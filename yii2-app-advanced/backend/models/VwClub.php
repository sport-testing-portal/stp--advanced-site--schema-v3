<?php

namespace app\models;

use Yii;
use \app\models\base\VwClub as BaseVwClub;

/**
 * This is the model class for table "vwClub".
 */
class VwClub extends BaseVwClub
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'org_type_id', 'created_by_user_id', 'updated_by_user_id'], 'integer'],
            [['org_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_type_name', 'org_state_or_region'], 'string', 'max' => 45],
            [['org_name', 'org_addr1', 'org_addr2', 'org_addr3'], 'string', 'max' => 100],
            [['org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['org_phone_main', 'org_postal_code'], 'string', 'max' => 25],
            [['org_email_main', 'org_city'], 'string', 'max' => 75],
            [['org_country_code_iso3'], 'string', 'max' => 3],
            [['org_created_by_username', 'org_updated_by_username'], 'string', 'max' => 131],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_type_name' => 'Org Type Name',
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'org_name' => 'Org Name',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'org_addr1' => 'Org Addr1',
            'org_addr2' => 'Org Addr2',
            'org_addr3' => 'Org Addr3',
            'org_city' => 'Org City',
            'org_state_or_region' => 'Org State Or Region',
            'org_postal_code' => 'Org Postal Code',
            'org_country_code_iso3' => 'Org Country Code Iso3',
            'org_created_by_username' => 'Org Created By Username',
            'org_updated_by_username' => 'Org Updated By Username',
            'created_by_user_id' => 'Created By User ID',
            'updated_by_user_id' => 'Updated By User ID',
        ];
    }
}
