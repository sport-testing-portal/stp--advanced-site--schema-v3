<?php

namespace app\models;

use Yii;
use \app\models\base\OrgSchool as BaseOrgSchool;

/**
 * This is the model class for table "org_school".
 */
class OrgSchool extends BaseOrgSchool
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'school_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_school_id' => 'Org School ID',
            'org_id' => 'Org ID',
            'school_id' => 'School ID',
            'lock' => 'Lock',
        ];
    }
}
