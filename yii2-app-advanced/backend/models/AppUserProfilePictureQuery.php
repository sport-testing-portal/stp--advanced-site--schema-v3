<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AppUserProfilePicture]].
 *
 * @see AppUserProfilePicture
 */
class AppUserProfilePictureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AppUserProfilePicture[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AppUserProfilePicture|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
