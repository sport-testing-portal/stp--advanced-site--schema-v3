<?php

namespace app\models;

use Yii;
use \app\models\base\PaymentLog as BasePaymentLog;

/**
 * This is the model class for table "payment_log".
 */
class PaymentLog extends BasePaymentLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['payment_type_id', 'data_entity_id', 'subscription_id', 'org_id', 'person_id', 'created_by', 'updated_by'], 'integer'],
            [['payment_amt'], 'number'],
            [['payment_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'payment_log_id' => 'Payment Log ID',
            'payment_type_id' => 'Payment Type ID',
            'data_entity_id' => 'Data Entity ID',
            'subscription_id' => 'Subscription ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'payment_amt' => 'Payment Amt',
            'payment_dt' => 'Payment Dt',
            'lock' => 'Lock',
        ];
    }
}
