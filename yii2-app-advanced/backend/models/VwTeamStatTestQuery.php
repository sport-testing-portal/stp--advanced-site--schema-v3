<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTeamStatTest]].
 *
 * @see VwTeamStatTest
 */
class VwTeamStatTestQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTeamStatTest[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTeamStatTest|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
