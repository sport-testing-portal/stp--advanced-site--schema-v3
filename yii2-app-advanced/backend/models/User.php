<?php

namespace app\models;

use Yii;
use \app\models\base\User as BaseUser;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['username', 'email', 'password_hash', 'auth_key', 'created_at', 'updated_at'], 'required'],
            [['created_on', 'updated_on', 'last_visit_on', 'password_set_on', 'srbac_set_on', 'person_date_of_birth'], 'safe'],
            [['one_time_password_counter', 'gender_id', 'confirmed_at', 'blocked_at', 'created_at', 'updated_at', 'flags', 'last_login_at'], 'integer'],
            [['username', 'password', 'email', 'activation_key', 'one_time_password_secret', 'one_time_password_code', 'unconfirmed_email'], 'string', 'max' => 255],
            [['firstname', 'lastname', 'person_addr_1', 'person_addr_2', 'person_city'], 'string', 'max' => 65],
            [['email_verified', 'is_active', 'is_disabled'], 'string', 'max' => 4],
            [['person_state_or_region', 'registration_ip'], 'string', 'max' => 45],
            [['person_postal_code'], 'string', 'max' => 18],
            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'activation_key' => 'Activation Key',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'last_visit_on' => 'Last Visit On',
            'password_set_on' => 'Password Set On',
            'email_verified' => 'Email Verified',
            'is_active' => 'Is Active',
            'is_disabled' => 'Is Disabled',
            'one_time_password_secret' => 'One Time Password Secret',
            'one_time_password_code' => 'One Time Password Code',
            'one_time_password_counter' => 'One Time Password Counter',
            'srbac_set_on' => 'Srbac Set On',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_city' => 'Person City',
            'person_state_or_region' => 'Person State Or Region',
            'person_postal_code' => 'Person Postal Code',
            'gender_id' => 'Gender ID',
            'person_date_of_birth' => 'Person Date Of Birth',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'confirmed_at' => 'Confirmed At',
            'unconfirmed_email' => 'Unconfirmed Email',
            'blocked_at' => 'Blocked At',
            'registration_ip' => 'Registration Ip',
            'flags' => 'Flags',
            'last_login_at' => 'Last Login At',
        ];
    }
}
