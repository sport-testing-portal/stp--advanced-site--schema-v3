<?php

namespace app\models;

use Yii;
use \app\models\base\AppUserLoginAttempt as BaseAppUserLoginAttempt;

/**
 * This is the model class for table "app_user_login_attempt".
 */
class AppUserLoginAttempt extends BaseAppUserLoginAttempt
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['app_user_id', 'ipv4'], 'integer'],
            [['performed_on'], 'safe'],
            [['username', 'session_id', 'user_agent'], 'string', 'max' => 255],
            [['is_successful'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_user_login_attempt_id' => 'App User Login Attempt ID',
            'app_user_id' => 'App User ID',
            'username' => 'Username',
            'performed_on' => 'Performed On',
            'is_successful' => 'Is Successful',
            'session_id' => 'Session ID',
            'ipv4' => 'Ipv4',
            'user_agent' => 'User Agent',
        ];
    }
}
