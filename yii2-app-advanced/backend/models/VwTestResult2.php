<?php

namespace app\models;

use Yii;
use \app\models\base\VwTestResult2 as BaseVwTestResult2;

/**
 * This is the model class for table "vwTestResult2".
 */
class VwTestResult2 extends BaseVwTestResult2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['score'], 'number'],
            [['overall_ranking', 'positional_ranking', 'person_id', 'player_id', 'test_eval_summary_log_id', 'test_eval_detail_log_id', 'source_event_id', 'source_record_id', 'import_file_id', 'import_file_line_num'], 'integer'],
            [['test_date_iso'], 'safe'],
            [['person'], 'string', 'max' => 90],
            [['ptype', 'test_desc', 'split', 'test_units', 'trial_status'], 'string', 'max' => 45],
            [['gender'], 'string', 'max' => 5],
            [['test_type', 'score_url', 'video_url', 'source_file_name'], 'string', 'max' => 150],
            [['test_date'], 'string', 'max' => 40],
            [['tester'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person' => 'Person',
            'ptype' => 'Ptype',
            'gender' => 'Gender',
            'test_desc' => 'Test Desc',
            'test_type' => 'Test Type',
            'split' => 'Split',
            'score' => 'Score',
            'test_units' => 'Test Units',
            'trial_status' => 'Trial Status',
            'overall_ranking' => 'Overall Ranking',
            'positional_ranking' => 'Positional Ranking',
            'score_url' => 'Score Url',
            'video_url' => 'Video Url',
            'test_date' => 'Test Date',
            'test_date_iso' => 'Test Date Iso',
            'tester' => 'Tester',
            'person_id' => 'Person ID',
            'player_id' => 'Player ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'test_eval_detail_log_id' => 'Test Eval Detail Log ID',
            'source_event_id' => 'Source Event ID',
            'source_record_id' => 'Source Record ID',
            'import_file_id' => 'Import File ID',
            'import_file_line_num' => 'Import File Line Num',
            'source_file_name' => 'Source File Name',
        ];
    }
}
