<?php

namespace app\models;

use Yii;
use \app\models\base\TestEvalDetailLog as BaseTestEvalDetailLog;

/**
 * This is the model class for table "test_eval_detail_log".
 */
class TestEvalDetailLog extends BaseTestEvalDetailLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['test_eval_summary_log_id', 'test_eval_detail_test_order_id', 'test_eval_detail_test_desc_id', 'test_eval_detail_score_int', 'test_eval_detail_rating', 'test_eval_detail_overall_ranking', 'test_eval_detail_positional_ranking', 'test_eval_detail_source_event_id', 'test_eval_detail_source_record_id', 'test_eval_detail_import_file_id', 'test_eval_detail_import_file_line_num', 'created_by', 'updated_by'], 'integer'],
            [['test_eval_detail_score_num'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['test_eval_detail_score_text', 'test_eval_detail_trial_status'], 'string', 'max' => 45],
            [['test_eval_detail_attempt'], 'string', 'max' => 4],
            [['test_eval_detail_score_url', 'test_eval_detail_video_url'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'test_eval_detail_log_id' => 'Test Eval Detail Log ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'test_eval_detail_test_order_id' => 'Test Eval Detail Test Order ID',
            'test_eval_detail_test_desc_id' => 'Test Eval Detail Test Desc ID',
            'test_eval_detail_score_text' => 'Test Eval Detail Score Text',
            'test_eval_detail_score_num' => 'Test Eval Detail Score Num',
            'test_eval_detail_attempt' => 'Test Eval Detail Attempt',
            'test_eval_detail_score_int' => 'Test Eval Detail Score Int',
            'test_eval_detail_score_url' => 'Test Eval Detail Score Url',
            'test_eval_detail_video_url' => 'Test Eval Detail Video Url',
            'test_eval_detail_rating' => 'Test Eval Detail Rating',
            'test_eval_detail_overall_ranking' => 'Test Eval Detail Overall Ranking',
            'test_eval_detail_positional_ranking' => 'Test Eval Detail Positional Ranking',
            'test_eval_detail_source_event_id' => 'Test Eval Detail Source Event ID',
            'test_eval_detail_source_record_id' => 'Test Eval Detail Source Record ID',
            'test_eval_detail_import_file_id' => 'Test Eval Detail Import File ID',
            'test_eval_detail_import_file_line_num' => 'Test Eval Detail Import File Line Num',
            'test_eval_detail_trial_status' => 'Test Eval Detail Trial Status',
            'lock' => 'Lock',
        ];
    }
}
