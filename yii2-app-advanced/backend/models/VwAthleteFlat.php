<?php

namespace app\models;

use Yii;
use \app\models\base\VwAthleteFlat as BaseVwAthleteFlat;

/**
 * This is the model class for table "vwAthleteFlat".
 */
class VwAthleteFlat extends BaseVwAthleteFlat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_age', 'person_bmi'], 'number'],
            [['player_id', 'player_person_id', 'person_id', 'person_org_id', 'person_app_user_id', 'person_person_type_id', 'person_user_id', 'player_age_group_id', 'player_team_player_id', 'player_sport_position_id', 'player_sport_position2_id', 'person_gender_id', 'person_weight', 'person_high_school__graduation_year', 'person_college_graduation_year', 'created_by', 'updated_by', 'person_type_id', 'gender_id', 'org_id', 'org_org_type_id', 'org_org_level_id'], 'integer'],
            [['player_waiver_minor_dt', 'player_waiver_adult_dt', 'person_date_of_birth', 'created_at', 'updated_at'], 'safe'],
            [['person_type_name'], 'required'],
            [['person_name_full', 'person_profile_url'], 'string', 'max' => 90],
            [['person_city', 'org_email_main', 'org_city'], 'string', 'max' => 75],
            [['person_state_or_region', 'player_sport_position_name', 'player_access_code', 'player_sport_position_preference', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_height_imperial', 'person_country', 'person_type_name', 'person_type_desc_short', 'person_type_desc_long', 'org_type_name', 'org_state_or_region'], 'string', 'max' => 45],
            [['gsm_staff'], 'string', 'max' => 65],
            [['player_parent_email', 'person_email_personal', 'person_email_work', 'person_profile_uri'], 'string', 'max' => 60],
            [['player_sport_preference'], 'string', 'max' => 35],
            [['player_shot_side_preference', 'person_name_prefix', 'person_postal_code', 'person_college_commitment_status', 'org_phone_main', 'org_postal_code'], 'string', 'max' => 25],
            [['player_dominant_side', 'player_dominant_foot', 'person_height', 'gender_code'], 'string', 'max' => 5],
            [['player_statistical_highlights'], 'string', 'max' => 300],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'org_name', 'org_addr1', 'org_addr2', 'org_addr3'], 'string', 'max' => 100],
            [['person_dob_eng', 'person_tshirt_size'], 'string', 'max' => 10],
            [['person_bday_long'], 'string', 'max' => 69],
            [['person_country_code', 'org_country_code_iso3'], 'string', 'max' => 3],
            [['gender_desc'], 'string', 'max' => 30],
            [['org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person_name_full' => 'Person Name Full',
            'person_age' => 'Person Age',
            'person_city' => 'Person City',
            'person_state_or_region' => 'Person State Or Region',
            'gsm_staff' => 'Gsm Staff',
            'player_id' => 'Player ID',
            'player_person_id' => 'Player Person ID',
            'person_id' => 'Person ID',
            'person_org_id' => 'Person Org ID',
            'person_app_user_id' => 'Person App User ID',
            'person_person_type_id' => 'Person Person Type ID',
            'person_user_id' => 'Person User ID',
            'player_age_group_id' => 'Player Age Group ID',
            'player_team_player_id' => 'Player Team Player ID',
            'player_sport_position_id' => 'Player Sport Position ID',
            'player_sport_position2_id' => 'Player Sport Position2 ID',
            'player_sport_position_name' => 'Player Sport Position Name',
            'player_access_code' => 'Player Access Code',
            'player_waiver_minor_dt' => 'Player Waiver Minor Dt',
            'player_waiver_adult_dt' => 'Player Waiver Adult Dt',
            'player_parent_email' => 'Player Parent Email',
            'player_sport_preference' => 'Player Sport Preference',
            'player_sport_position_preference' => 'Player Sport Position Preference',
            'player_shot_side_preference' => 'Player Shot Side Preference',
            'player_dominant_side' => 'Player Dominant Side',
            'player_dominant_foot' => 'Player Dominant Foot',
            'player_statistical_highlights' => 'Player Statistical Highlights',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'person_gender_id' => 'Person Gender ID',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_dob_eng' => 'Person Dob Eng',
            'person_bday_long' => 'Person Bday Long',
            'person_height' => 'Person Height',
            'person_height_imperial' => 'Person Height Imperial',
            'person_weight' => 'Person Weight',
            'person_bmi' => 'Person Bmi',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_profile_url' => 'Person Profile Url',
            'person_profile_uri' => 'Person Profile Uri',
            'person_high_school__graduation_year' => 'Person High School  Graduation Year',
            'person_college_graduation_year' => 'Person College Graduation Year',
            'person_college_commitment_status' => 'Person College Commitment Status',
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'person_type_desc_short' => 'Person Type Desc Short',
            'person_type_desc_long' => 'Person Type Desc Long',
            'gender_id' => 'Gender ID',
            'gender_desc' => 'Gender Desc',
            'gender_code' => 'Gender Code',
            'org_id' => 'Org ID',
            'org_org_type_id' => 'Org Org Type ID',
            'org_org_level_id' => 'Org Org Level ID',
            'org_name' => 'Org Name',
            'org_type_name' => 'Org Type Name',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'org_addr1' => 'Org Addr1',
            'org_addr2' => 'Org Addr2',
            'org_addr3' => 'Org Addr3',
            'org_city' => 'Org City',
            'org_state_or_region' => 'Org State Or Region',
            'org_postal_code' => 'Org Postal Code',
            'org_country_code_iso3' => 'Org Country Code Iso3',
        ];
    }
}
