<?php

namespace app\models;

use Yii;
use \app\models\base\SchoolStat as BaseSchoolStat;

/**
 * This is the model class for table "school_stat".
 */
class SchoolStat extends BaseSchoolStat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['school_id', 'college_cost_rating', 'college_cost_tuition_instate', 'college_cost_tuition_outofstate', 'college_cost_room_and_board', 'college_cost_books_and_supplies', 'college_pct_applicants_admitted', 'created_by', 'updated_by'], 'integer'],
            [['college_early_decision_dt', 'college_early_action_dt', 'college_regular_decision_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'school_stat_id' => 'School Stat ID',
            'school_id' => 'School ID',
            'college_cost_rating' => 'College Cost Rating',
            'college_cost_tuition_instate' => 'College Cost Tuition Instate',
            'college_cost_tuition_outofstate' => 'College Cost Tuition Outofstate',
            'college_cost_room_and_board' => 'College Cost Room And Board',
            'college_cost_books_and_supplies' => 'College Cost Books And Supplies',
            'college_early_decision_dt' => 'College Early Decision Dt',
            'college_early_action_dt' => 'College Early Action Dt',
            'college_regular_decision_dt' => 'College Regular Decision Dt',
            'college_pct_applicants_admitted' => 'College Pct Applicants Admitted',
            'lock' => 'Lock',
        ];
    }
}
