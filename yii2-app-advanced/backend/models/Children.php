<?php

namespace app\models;

use Yii;
use \app\models\base\Children as BaseChildren;

/**
 * This is the model class for table "children".
 */
class Children extends BaseChildren
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['father_id', 'name', 'age'], 'required'],
            [['father_id', 'age'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'father_id' => 'Father ID',
            'name' => 'Name',
            'age' => 'Age',
        ];
    }
}
