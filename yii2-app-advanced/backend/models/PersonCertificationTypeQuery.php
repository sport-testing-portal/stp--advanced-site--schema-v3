<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PersonCertificationType]].
 *
 * @see PersonCertificationType
 */
class PersonCertificationTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PersonCertificationType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PersonCertificationType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
