<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AppParm]].
 *
 * @see AppParm
 */
class AppParmQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AppParm[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AppParm|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
