<?php

namespace app\models;

use Yii;
use \app\models\base\VwSchool as BaseVwSchool;

/**
 * This is the model class for table "vwSchool".
 */
class VwSchool extends BaseVwSchool
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_type', 'school_name'], 'required'],
            [['school_unit_id', 'conference_id', 'org_id', 'org_type_id', 'school_id'], 'integer'],
            [['updated_at'], 'safe'],
            [['org_type', 'school_state'], 'string', 'max' => 45],
            [['school_name', 'school_addr1', 'school_addr2'], 'string', 'max' => 100],
            [['school_city'], 'string', 'max' => 75],
            [['school_postal_code'], 'string', 'max' => 25],
            [['school_website'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_type' => 'Org Type',
            'school_name' => 'School Name',
            'school_addr1' => 'School Addr1',
            'school_addr2' => 'School Addr2',
            'school_city' => 'School City',
            'school_state' => 'School State',
            'school_postal_code' => 'School Postal Code',
            'school_website' => 'School Website',
            'school_unit_id' => 'School Unit ID',
            'conference_id' => 'Conference ID',
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'school_id' => 'School ID',
        ];
    }
}
