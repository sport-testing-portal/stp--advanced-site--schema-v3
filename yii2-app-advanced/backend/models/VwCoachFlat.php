<?php

namespace app\models;

use Yii;
use \app\models\base\VwCoachFlat as BaseVwCoachFlat;

/**
 * This is the model class for table "vwCoachFlat".
 */
class VwCoachFlat extends BaseVwCoachFlat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'person_id', 'org_id', 'coach_id', 'app_user_id', 'person_type_id', 'coach_type_id', 'coach_team_coach_id', 'org_type_id', 'gender_id', 'person_weight', 'person_high_school__graduation_year', 'person_college_graduation_year', 'created_by', 'updated_by', 'age_group_id', 'coach_created_by', 'coach_updated_by', 'org_level_id'], 'integer'],
            [['person_date_of_birth', 'org_affiliation_begin_dt', 'org_affiliation_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['username', 'person_website', 'person_created_by_username', 'person_updated_by_username', 'coach_created_by_username', 'coach_updated_by_username'], 'string', 'max' => 255],
            [['person_type_name', 'org_type_name', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_country', 'person_state_or_region', 'org_governing_body', 'org_state_or_region'], 'string', 'max' => 45],
            [['coach_type_name', 'person_city', 'org_level_name', 'org_email_main', 'org_city'], 'string', 'max' => 75],
            [['org_name', 'person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'coach_info_source_scrape_url', 'org_addr1', 'org_addr2', 'org_addr3'], 'string', 'max' => 100],
            [['person_name_prefix', 'person_college_commitment_status', 'person_postal_code', 'org_ncaa_clearing_house_id', 'org_phone_main', 'org_postal_code'], 'string', 'max' => 25],
            [['person_name_full', 'person_profile_url'], 'string', 'max' => 90],
            [['gender_code', 'person_height'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['person_email_personal', 'person_email_work', 'person_profile_uri'], 'string', 'max' => 60],
            [['person_tshirt_size'], 'string', 'max' => 10],
            [['person_country_code', 'org_country_code_iso3'], 'string', 'max' => 3],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri', 'org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['coach_comments'], 'string', 'max' => 253],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'Username',
            'person_id' => 'Person ID',
            'org_id' => 'Org ID',
            'coach_id' => 'Coach ID',
            'app_user_id' => 'App User ID',
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'coach_type_id' => 'Coach Type ID',
            'coach_type_name' => 'Coach Type Name',
            'coach_team_coach_id' => 'Coach Team Coach ID',
            'org_name' => 'Org Name',
            'org_type_id' => 'Org Type ID',
            'org_type_name' => 'Org Type Name',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'gender_id' => 'Gender ID',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_height' => 'Person Height',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_high_school__graduation_year' => 'Person High School  Graduation Year',
            'person_college_graduation_year' => 'Person College Graduation Year',
            'person_college_commitment_status' => 'Person College Commitment Status',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'org_affiliation_begin_dt' => 'Org Affiliation Begin Dt',
            'org_affiliation_end_dt' => 'Org Affiliation End Dt',
            'person_website' => 'Person Website',
            'person_profile_url' => 'Person Profile Url',
            'person_profile_uri' => 'Person Profile Uri',
            'person_created_by_username' => 'Person Created By Username',
            'person_updated_by_username' => 'Person Updated By Username',
            'age_group_id' => 'Age Group ID',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_comments' => 'Coach Comments',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
            'coach_created_by_username' => 'Coach Created By Username',
            'coach_created_by' => 'Coach Created By',
            'coach_updated_by_username' => 'Coach Updated By Username',
            'coach_updated_by' => 'Coach Updated By',
            'org_level_id' => 'Org Level ID',
            'org_level_name' => 'Org Level Name',
            'org_governing_body' => 'Org Governing Body',
            'org_ncaa_clearing_house_id' => 'Org Ncaa Clearing House ID',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'org_addr1' => 'Org Addr1',
            'org_addr2' => 'Org Addr2',
            'org_addr3' => 'Org Addr3',
            'org_city' => 'Org City',
            'org_state_or_region' => 'Org State Or Region',
            'org_postal_code' => 'Org Postal Code',
            'org_country_code_iso3' => 'Org Country Code Iso3',
        ];
    }
}
