<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ImportFileStiSpecialReportCombo]].
 *
 * @see ImportFileStiSpecialReportCombo
 */
class ImportFileStiSpecialReportComboQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ImportFileStiSpecialReportCombo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ImportFileStiSpecialReportCombo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
