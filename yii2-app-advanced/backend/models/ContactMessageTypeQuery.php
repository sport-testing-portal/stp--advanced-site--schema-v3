<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ContactMessageType]].
 *
 * @see ContactMessageType
 */
class ContactMessageTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ContactMessageType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ContactMessageType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
