<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "payment_xfer".
 *
 * @property integer $payment_xfer_id
 * @property integer $payment_log_id
 * @property integer $payment_xfer_entity_id
 * @property string $payment_xfer_status_code_char
 * @property integer $payment_xfer_status_code_num
 * @property string $payment_xfer_sent_dt
 * @property string $payment_xfer_recd_status_dt
 * @property string $payment_xfer_blob_uri
 * @property string $payment_xfer_blob_format
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\PaymentLog $paymentLog
 * @property \app\models\PaymentXferEntity $paymentXferEntity
 */
class PaymentXfer extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'paymentLog',
            'paymentXferEntity'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_log_id', 'payment_xfer_entity_id', 'payment_xfer_status_code_num', 'created_by', 'updated_by'], 'integer'],
            [['payment_xfer_sent_dt', 'payment_xfer_recd_status_dt', 'created_at', 'updated_at'], 'safe'],
            [['payment_xfer_status_code_char', 'payment_xfer_blob_format'], 'string', 'max' => 45],
            [['payment_xfer_blob_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_xfer';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_xfer_id' => 'Payment Xfer ID',
            'payment_log_id' => 'Payment Log ID',
            'payment_xfer_entity_id' => 'Payment Xfer Entity ID',
            'payment_xfer_status_code_char' => 'Payment Xfer Status Code Char',
            'payment_xfer_status_code_num' => 'Payment Xfer Status Code Num',
            'payment_xfer_sent_dt' => 'Payment Xfer Sent Dt',
            'payment_xfer_recd_status_dt' => 'Payment Xfer Recd Status Dt',
            'payment_xfer_blob_uri' => 'Payment Xfer Blob Uri',
            'payment_xfer_blob_format' => 'Payment Xfer Blob Format',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentLog()
    {
        return $this->hasOne(\app\models\PaymentLog::className(), ['payment_log_id' => 'payment_log_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentXferEntity()
    {
        return $this->hasOne(\app\models\PaymentXferEntity::className(), ['payment_xfer_entity_id' => 'payment_xfer_entity_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PaymentXferQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PaymentXferQuery(get_called_class());
    }
}
