<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "notification_template".
 *
 * @property integer $notification_template_id
 * @property integer $notification_template_type_id
 * @property string $notification_template_uri
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\NotificationLog[] $notificationLogs
 * @property \app\models\NotificationTemplateType $notificationTemplateType
 */
class NotificationTemplate extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'notificationLogs',
            'notificationTemplateType'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_template_type_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['notification_template_uri'], 'string', 'max' => 256],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_template';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_template_id' => 'Notification Template ID',
            'notification_template_type_id' => 'Notification Template Type ID',
            'notification_template_uri' => 'Notification Template Uri',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationLogs()
    {
        return $this->hasMany(\app\models\NotificationLog::className(), ['notification_template_id' => 'notification_template_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationTemplateType()
    {
        return $this->hasOne(\app\models\NotificationTemplateType::className(), ['notification_template_type_id' => 'notification_template_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\NotificationTemplateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\NotificationTemplateQuery(get_called_class());
    }
}
