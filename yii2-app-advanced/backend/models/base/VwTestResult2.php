<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwTestResult2".
 *
 * @property string $person
 * @property string $ptype
 * @property string $gender
 * @property string $test_desc
 * @property string $test_type
 * @property string $split
 * @property string $score
 * @property string $test_units
 * @property string $trial_status
 * @property integer $overall_ranking
 * @property integer $positional_ranking
 * @property string $score_url
 * @property string $video_url
 * @property string $test_date
 * @property string $test_date_iso
 * @property string $tester
 * @property integer $person_id
 * @property integer $player_id
 * @property integer $test_eval_summary_log_id
 * @property integer $test_eval_detail_log_id
 * @property integer $source_event_id
 * @property integer $source_record_id
 * @property integer $import_file_id
 * @property integer $import_file_line_num
 * @property string $source_file_name
 */
class VwTestResult2 extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['score'], 'number'],
            [['overall_ranking', 'positional_ranking', 'person_id', 'player_id', 'test_eval_summary_log_id', 'test_eval_detail_log_id', 'source_event_id', 'source_record_id', 'import_file_id', 'import_file_line_num'], 'integer'],
            [['test_date_iso'], 'safe'],
            [['person'], 'string', 'max' => 90],
            [['ptype', 'test_desc', 'split', 'test_units', 'trial_status'], 'string', 'max' => 45],
            [['gender'], 'string', 'max' => 5],
            [['test_type', 'score_url', 'video_url', 'source_file_name'], 'string', 'max' => 150],
            [['test_date'], 'string', 'max' => 40],
            [['tester'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwTestResult2';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person' => 'Person',
            'ptype' => 'Ptype',
            'gender' => 'Gender',
            'test_desc' => 'Test Desc',
            'test_type' => 'Test Type',
            'split' => 'Split',
            'score' => 'Score',
            'test_units' => 'Test Units',
            'trial_status' => 'Trial Status',
            'overall_ranking' => 'Overall Ranking',
            'positional_ranking' => 'Positional Ranking',
            'score_url' => 'Score Url',
            'video_url' => 'Video Url',
            'test_date' => 'Test Date',
            'test_date_iso' => 'Test Date Iso',
            'tester' => 'Tester',
            'person_id' => 'Person ID',
            'player_id' => 'Player ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'test_eval_detail_log_id' => 'Test Eval Detail Log ID',
            'source_event_id' => 'Source Event ID',
            'source_record_id' => 'Source Record ID',
            'import_file_id' => 'Import File ID',
            'import_file_line_num' => 'Import File Line Num',
            'source_file_name' => 'Source File Name',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwTestResult2Query the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwTestResult2Query(get_called_class());
    }
}
