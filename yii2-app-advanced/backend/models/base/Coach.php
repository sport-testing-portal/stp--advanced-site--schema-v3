<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "coach".
 *
 * @property integer $coach_id
 * @property integer $person_id
 * @property integer $coach_type_id
 * @property integer $coach_team_coach_id
 * @property integer $age_group_id
 * @property string $coach_specialty
 * @property string $coach_certifications
 * @property string $coach_comments
 * @property string $coach_qrcode_uri
 * @property string $coach_info_source_scrape_url
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\CampCoach[] $campCoaches
 * @property \app\models\CampSessionCoach[] $campSessionCoaches
 * @property \app\models\CampSessionPlayerEval[] $campSessionPlayerEvals
 * @property \app\models\AgeGroup $ageGroup
 * @property \app\models\CoachType $coachType
 * @property \app\models\Person $person
 * @property \app\models\TeamCoach $coachTeamCoach
 * @property \app\models\CoachSport[] $coachSports
 * @property \app\models\SchoolCoach[] $schoolCoaches
 * @property \app\models\TeamCoach[] $teamCoaches
 */
class Coach extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'campCoaches',
            'campSessionCoaches',
            'campSessionPlayerEvals',
            'ageGroup',
            'coachType',
            'person',
            'coachTeamCoach',
            'coachSports',
            'schoolCoaches',
            'teamCoaches'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'coach_type_id', 'coach_team_coach_id', 'age_group_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri'], 'string', 'max' => 150],
            [['coach_comments'], 'string', 'max' => 253],
            [['coach_info_source_scrape_url'], 'string', 'max' => 100],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coach';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'coach_id' => 'Coach ID',
            'person_id' => 'Person ID',
            'coach_type_id' => 'Coach Type ID',
            'coach_team_coach_id' => 'Coach Team Coach ID',
            'age_group_id' => 'Age Group ID',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_comments' => 'Coach Comments',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampCoaches()
    {
        return $this->hasMany(\app\models\CampCoach::className(), ['coach_id' => 'coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionCoaches()
    {
        return $this->hasMany(\app\models\CampSessionCoach::className(), ['coach_id' => 'coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionPlayerEvals()
    {
        return $this->hasMany(\app\models\CampSessionPlayerEval::className(), ['coach_id' => 'coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeGroup()
    {
        return $this->hasOne(\app\models\AgeGroup::className(), ['age_group_id' => 'age_group_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachType()
    {
        return $this->hasOne(\app\models\CoachType::className(), ['coach_type_id' => 'coach_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachTeamCoach()
    {
        return $this->hasOne(\app\models\TeamCoach::className(), ['team_coach_id' => 'coach_team_coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachSports()
    {
        return $this->hasMany(\app\models\CoachSport::className(), ['coach_id' => 'coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolCoaches()
    {
        return $this->hasMany(\app\models\SchoolCoach::className(), ['coach_id' => 'coach_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamCoaches()
    {
        return $this->hasMany(\app\models\TeamCoach::className(), ['coach_id' => 'coach_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\CoachQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CoachQuery(get_called_class());
    }
}
