<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwAthleteFlat".
 *
 * @property string $person_name_full
 * @property double $person_age
 * @property string $person_city
 * @property string $person_state_or_region
 * @property string $gsm_staff
 * @property integer $player_id
 * @property integer $player_person_id
 * @property integer $person_id
 * @property integer $person_org_id
 * @property integer $person_app_user_id
 * @property integer $person_person_type_id
 * @property integer $person_user_id
 * @property integer $player_age_group_id
 * @property integer $player_team_player_id
 * @property integer $player_sport_position_id
 * @property integer $player_sport_position2_id
 * @property string $player_sport_position_name
 * @property string $player_access_code
 * @property string $player_waiver_minor_dt
 * @property string $player_waiver_adult_dt
 * @property string $player_parent_email
 * @property string $player_sport_preference
 * @property string $player_sport_position_preference
 * @property string $player_shot_side_preference
 * @property string $player_dominant_side
 * @property string $player_dominant_foot
 * @property string $player_statistical_highlights
 * @property string $person_name_prefix
 * @property string $person_name_first
 * @property string $person_name_middle
 * @property string $person_name_last
 * @property string $person_name_suffix
 * @property string $person_phone_personal
 * @property string $person_email_personal
 * @property string $person_phone_work
 * @property string $person_email_work
 * @property string $person_position_work
 * @property integer $person_gender_id
 * @property string $person_image_headshot_url
 * @property string $person_name_nickname
 * @property string $person_date_of_birth
 * @property string $person_dob_eng
 * @property string $person_bday_long
 * @property string $person_height
 * @property string $person_height_imperial
 * @property integer $person_weight
 * @property double $person_bmi
 * @property string $person_tshirt_size
 * @property string $person_addr_1
 * @property string $person_addr_2
 * @property string $person_addr_3
 * @property string $person_postal_code
 * @property string $person_country
 * @property string $person_country_code
 * @property string $person_profile_url
 * @property string $person_profile_uri
 * @property integer $person_high_school__graduation_year
 * @property integer $person_college_graduation_year
 * @property string $person_college_commitment_status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $person_type_id
 * @property string $person_type_name
 * @property string $person_type_desc_short
 * @property string $person_type_desc_long
 * @property integer $gender_id
 * @property string $gender_desc
 * @property string $gender_code
 * @property integer $org_id
 * @property integer $org_org_type_id
 * @property integer $org_org_level_id
 * @property string $org_name
 * @property string $org_type_name
 * @property string $org_website_url
 * @property string $org_twitter_url
 * @property string $org_facebook_url
 * @property string $org_phone_main
 * @property string $org_email_main
 * @property string $org_addr1
 * @property string $org_addr2
 * @property string $org_addr3
 * @property string $org_city
 * @property string $org_state_or_region
 * @property string $org_postal_code
 * @property string $org_country_code_iso3
 */
class VwAthleteFlat extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_age', 'person_bmi'], 'number'],
            [['player_id', 'player_person_id', 'person_id', 'person_org_id', 'person_app_user_id', 'person_person_type_id', 'person_user_id', 'player_age_group_id', 'player_team_player_id', 'player_sport_position_id', 'player_sport_position2_id', 'person_gender_id', 'person_weight', 'person_high_school__graduation_year', 'person_college_graduation_year', 'created_by', 'updated_by', 'person_type_id', 'gender_id', 'org_id', 'org_org_type_id', 'org_org_level_id'], 'integer'],
            [['player_waiver_minor_dt', 'player_waiver_adult_dt', 'person_date_of_birth', 'created_at', 'updated_at'], 'safe'],
            [['person_type_name'], 'required'],
            [['person_name_full', 'person_profile_url'], 'string', 'max' => 90],
            [['person_city', 'org_email_main', 'org_city'], 'string', 'max' => 75],
            [['person_state_or_region', 'player_sport_position_name', 'player_access_code', 'player_sport_position_preference', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_height_imperial', 'person_country', 'person_type_name', 'person_type_desc_short', 'person_type_desc_long', 'org_type_name', 'org_state_or_region'], 'string', 'max' => 45],
            [['gsm_staff'], 'string', 'max' => 65],
            [['player_parent_email', 'person_email_personal', 'person_email_work', 'person_profile_uri'], 'string', 'max' => 60],
            [['player_sport_preference'], 'string', 'max' => 35],
            [['player_shot_side_preference', 'person_name_prefix', 'person_postal_code', 'person_college_commitment_status', 'org_phone_main', 'org_postal_code'], 'string', 'max' => 25],
            [['player_dominant_side', 'player_dominant_foot', 'person_height', 'gender_code'], 'string', 'max' => 5],
            [['player_statistical_highlights'], 'string', 'max' => 300],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'org_name', 'org_addr1', 'org_addr2', 'org_addr3'], 'string', 'max' => 100],
            [['person_dob_eng', 'person_tshirt_size'], 'string', 'max' => 10],
            [['person_bday_long'], 'string', 'max' => 69],
            [['person_country_code', 'org_country_code_iso3'], 'string', 'max' => 3],
            [['gender_desc'], 'string', 'max' => 30],
            [['org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwAthleteFlat';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_name_full' => 'Person Name Full',
            'person_age' => 'Person Age',
            'person_city' => 'Person City',
            'person_state_or_region' => 'Person State Or Region',
            'gsm_staff' => 'Gsm Staff',
            'player_id' => 'Player ID',
            'player_person_id' => 'Player Person ID',
            'person_id' => 'Person ID',
            'person_org_id' => 'Person Org ID',
            'person_app_user_id' => 'Person App User ID',
            'person_person_type_id' => 'Person Person Type ID',
            'person_user_id' => 'Person User ID',
            'player_age_group_id' => 'Player Age Group ID',
            'player_team_player_id' => 'Player Team Player ID',
            'player_sport_position_id' => 'Player Sport Position ID',
            'player_sport_position2_id' => 'Player Sport Position2 ID',
            'player_sport_position_name' => 'Player Sport Position Name',
            'player_access_code' => 'Player Access Code',
            'player_waiver_minor_dt' => 'Player Waiver Minor Dt',
            'player_waiver_adult_dt' => 'Player Waiver Adult Dt',
            'player_parent_email' => 'Player Parent Email',
            'player_sport_preference' => 'Player Sport Preference',
            'player_sport_position_preference' => 'Player Sport Position Preference',
            'player_shot_side_preference' => 'Player Shot Side Preference',
            'player_dominant_side' => 'Player Dominant Side',
            'player_dominant_foot' => 'Player Dominant Foot',
            'player_statistical_highlights' => 'Player Statistical Highlights',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'person_gender_id' => 'Person Gender ID',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_dob_eng' => 'Person Dob Eng',
            'person_bday_long' => 'Person Bday Long',
            'person_height' => 'Person Height',
            'person_height_imperial' => 'Person Height Imperial',
            'person_weight' => 'Person Weight',
            'person_bmi' => 'Person Bmi',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_profile_url' => 'Person Profile Url',
            'person_profile_uri' => 'Person Profile Uri',
            'person_high_school__graduation_year' => 'Person High School  Graduation Year',
            'person_college_graduation_year' => 'Person College Graduation Year',
            'person_college_commitment_status' => 'Person College Commitment Status',
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'person_type_desc_short' => 'Person Type Desc Short',
            'person_type_desc_long' => 'Person Type Desc Long',
            'gender_id' => 'Gender ID',
            'gender_desc' => 'Gender Desc',
            'gender_code' => 'Gender Code',
            'org_id' => 'Org ID',
            'org_org_type_id' => 'Org Org Type ID',
            'org_org_level_id' => 'Org Org Level ID',
            'org_name' => 'Org Name',
            'org_type_name' => 'Org Type Name',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'org_addr1' => 'Org Addr1',
            'org_addr2' => 'Org Addr2',
            'org_addr3' => 'Org Addr3',
            'org_city' => 'Org City',
            'org_state_or_region' => 'Org State Or Region',
            'org_postal_code' => 'Org Postal Code',
            'org_country_code_iso3' => 'Org Country Code Iso3',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwAthleteFlatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwAthleteFlatQuery(get_called_class());
    }
}
