<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "camp_session".
 *
 * @property integer $camp_session_id
 * @property integer $camp_id
 * @property integer $sport_id
 * @property integer $gender_id
 * @property integer $address_id
 * @property integer $season_id
 * @property string $camp_session_location
 * @property string $camp_session_url
 * @property string $camp_session_ages
 * @property string $camp_session_skill_level
 * @property string $camp_session_type
 * @property string $camp_session_begin_dt
 * @property string $camp_session_end_dt
 * @property string $camp_session_specialty
 * @property string $camp_session_scholarships_available_yn
 * @property string $camp_session_description
 * @property integer $camp_session_cost_regular_residential
 * @property integer $camp_session_cost_regular_commuter
 * @property integer $camp_session_cost_regular_day
 * @property integer $camp_session_cost_early_residential
 * @property integer $camp_session_cost_early_commuter
 * @property integer $camp_session_cost_early_day
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Address $address
 * @property \app\models\Camp $camp
 * @property \app\models\Gender $gender
 * @property \app\models\Season $season
 * @property \app\models\Sport $sport
 * @property \app\models\CampSessionCoach[] $campSessionCoaches
 * @property \app\models\CampSessionLocation[] $campSessionLocations
 * @property \app\models\CampSessionPlayerEval[] $campSessionPlayerEvals
 * @property \app\models\CampSessionSport[] $campSessionSports
 * @property \app\models\PlayerCampLog[] $playerCampLogs
 */
class CampSession extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'address',
            'camp',
            'gender',
            'season',
            'sport',
            'campSessionCoaches',
            'campSessionLocations',
            'campSessionPlayerEvals',
            'campSessionSports',
            'playerCampLogs'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_id', 'sport_id', 'gender_id', 'address_id', 'season_id', 'camp_session_cost_regular_residential', 'camp_session_cost_regular_commuter', 'camp_session_cost_regular_day', 'camp_session_cost_early_residential', 'camp_session_cost_early_commuter', 'camp_session_cost_early_day', 'created_by', 'updated_by'], 'integer'],
            [['camp_session_begin_dt', 'camp_session_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['camp_session_location', 'camp_session_url', 'camp_session_ages', 'camp_session_skill_level', 'camp_session_type', 'camp_session_specialty', 'camp_session_description'], 'string', 'max' => 45],
            [['camp_session_scholarships_available_yn', 'lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'camp_session';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'camp_session_id' => 'Camp Session ID',
            'camp_id' => 'Camp ID',
            'sport_id' => 'Sport ID',
            'gender_id' => 'Gender ID',
            'address_id' => 'Address ID',
            'season_id' => 'Season ID',
            'camp_session_location' => 'Camp Session Location',
            'camp_session_url' => 'Camp Session Url',
            'camp_session_ages' => 'Camp Session Ages',
            'camp_session_skill_level' => 'Camp Session Skill Level',
            'camp_session_type' => 'Camp Session Type',
            'camp_session_begin_dt' => 'Camp Session Begin Dt',
            'camp_session_end_dt' => 'Camp Session End Dt',
            'camp_session_specialty' => 'Camp Session Specialty',
            'camp_session_scholarships_available_yn' => 'Camp Session Scholarships Available Yn',
            'camp_session_description' => 'Camp Session Description',
            'camp_session_cost_regular_residential' => 'Camp Session Cost Regular Residential',
            'camp_session_cost_regular_commuter' => 'Camp Session Cost Regular Commuter',
            'camp_session_cost_regular_day' => 'Camp Session Cost Regular Day',
            'camp_session_cost_early_residential' => 'Camp Session Cost Early Residential',
            'camp_session_cost_early_commuter' => 'Camp Session Cost Early Commuter',
            'camp_session_cost_early_day' => 'Camp Session Cost Early Day',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(\app\models\Address::className(), ['address_id' => 'address_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCamp()
    {
        return $this->hasOne(\app\models\Camp::className(), ['camp_id' => 'camp_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender()
    {
        return $this->hasOne(\app\models\Gender::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(\app\models\Season::className(), ['season_id' => 'season_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(\app\models\Sport::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionCoaches()
    {
        return $this->hasMany(\app\models\CampSessionCoach::className(), ['camp_session_id' => 'camp_session_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionLocations()
    {
        return $this->hasMany(\app\models\CampSessionLocation::className(), ['camp_session_id' => 'camp_session_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionPlayerEvals()
    {
        return $this->hasMany(\app\models\CampSessionPlayerEval::className(), ['camp_session_id' => 'camp_session_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionSports()
    {
        return $this->hasMany(\app\models\CampSessionSport::className(), ['camp_session_id' => 'camp_session_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerCampLogs()
    {
        return $this->hasMany(\app\models\PlayerCampLog::className(), ['camp_session_id' => 'camp_session_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\CampSessionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CampSessionQuery(get_called_class());
    }
}
