<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "user_login_attempt".
 *
 * @property integer $id
 * @property string $username
 * @property integer $user_id
 * @property string $performed_on
 * @property integer $is_successful
 * @property string $session_id
 * @property integer $ipv4
 * @property string $user_agent
 *
 * @property \app\models\User $user
 */
class UserLoginAttempt extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'user'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'user_id'], 'required'],
            [['user_id', 'ipv4'], 'integer'],
            [['performed_on'], 'safe'],
            [['username', 'session_id', 'user_agent'], 'string', 'max' => 255],
            [['is_successful'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_login_attempt';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'user_id' => 'User ID',
            'performed_on' => 'Performed On',
            'is_successful' => 'Is Successful',
            'session_id' => 'Session ID',
            'ipv4' => 'Ipv4',
            'user_agent' => 'User Agent',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\UserLoginAttemptQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\UserLoginAttemptQuery(get_called_class());
    }
}
