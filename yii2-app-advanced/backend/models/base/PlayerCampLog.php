<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "player_camp_log".
 *
 * @property integer $player_camp_log_id
 * @property integer $player_id
 * @property integer $camp_id
 * @property integer $camp_session_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Camp $camp
 * @property \app\models\Player $player
 * @property \app\models\CampSession $campSession
 */
class PlayerCampLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'camp',
            'player',
            'campSession'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'camp_id', 'camp_session_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_camp_log';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'player_camp_log_id' => 'Player Camp Log ID',
            'player_id' => 'Player ID',
            'camp_id' => 'Camp ID',
            'camp_session_id' => 'Camp Session ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCamp()
    {
        return $this->hasOne(\app\models\Camp::className(), ['camp_id' => 'camp_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(\app\models\Player::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSession()
    {
        return $this->hasOne(\app\models\CampSession::className(), ['camp_session_id' => 'camp_session_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayerCampLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PlayerCampLogQuery(get_called_class());
    }
}
