<?php

namespace app\models;

use Yii;
use \app\models\base\NotificationTemplate as BaseNotificationTemplate;

/**
 * This is the model class for table "notification_template".
 */
class NotificationTemplate extends BaseNotificationTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['notification_template_type_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['notification_template_uri'], 'string', 'max' => 256],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'notification_template_id' => 'Notification Template ID',
            'notification_template_type_id' => 'Notification Template Type ID',
            'notification_template_uri' => 'Notification Template Uri',
            'lock' => 'Lock',
        ];
    }
}
