<?php

namespace app\models;

use Yii;
use \app\models\base\School as BaseSchool;

/**
 * This is the model class for table "school".
 */
class School extends BaseSchool
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'religious_affiliation_id', 'conference_id__female', 'conference_id__male', 'conference_id__main', 'school_ipeds_id', 'school_unit_id', 'school_ope_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'school_id' => 'School ID',
            'org_id' => 'Org ID',
            'religious_affiliation_id' => 'Religious Affiliation ID',
            'conference_id__female' => 'Conference Id  Female',
            'conference_id__male' => 'Conference Id  Male',
            'conference_id__main' => 'Conference Id  Main',
            'school_ipeds_id' => 'School Ipeds ID',
            'school_unit_id' => 'School Unit ID',
            'school_ope_id' => 'School Ope ID',
            'lock' => 'Lock',
        ];
    }
}
