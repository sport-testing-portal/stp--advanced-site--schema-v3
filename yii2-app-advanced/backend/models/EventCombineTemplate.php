<?php

namespace app\models;

use Yii;
use \app\models\base\EventCombineTemplate as BaseEventCombineTemplate;

/**
 * This is the model class for table "event_combine_template".
 */
class EventCombineTemplate extends BaseEventCombineTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'event_combine_template_id' => 'Event Combine Template ID',
            'lock' => 'Lock',
        ];
    }
}
