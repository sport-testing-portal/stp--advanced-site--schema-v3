<?php

namespace app\models;

use Yii;
use \app\models\base\VwTableRowCountV00 as BaseVwTableRowCountV00;

/**
 * This is the model class for table "vwTableRowCount_v00".
 */
class VwTableRowCountV00 extends BaseVwTableRowCountV00
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['table_name'], 'string', 'max' => 15],
            [['cnt'], 'string', 'max' => 61],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'table_name' => 'Table Name',
            'cnt' => 'Cnt',
        ];
    }
}
